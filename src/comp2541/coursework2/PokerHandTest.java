package comp2541.coursework2;

/**
 * @author Dimitar Arabadzhiyski
 *
 */

import static org.junit.Assert.*;

import java.awt.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class PokerHandTest {

	private Card twoSpades;
	private Card threeSpades;
	private Card fourSpades;
	private Card fiveSpades;
	private Card aceSpades;
	private Card aceHearts;
	private Card aceClubs;
	private List cards;
	private PokerHand handCards = new PokerHand();

	/**
	 * Setting up the cards that we are going to use for the tests
	 */
	@Before
	public void setUp() {
		twoSpades = new Card("2S");
		threeSpades = new Card("3S");
		fourSpades = new Card("4S");
		fiveSpades = new Card("5S");
		aceSpades = new Card("AS");
		aceHearts = new Card("AH");
		aceClubs = new Card("AC");
		new Card("6S");
	}

	/**
	 * Tests add card method
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAdd() {
		for (int i = 0; i < 6; i++) {
			handCards.add(twoSpades);
		}
	}

	/**
	 * Test to see if ToString method works
	 */
	@Test
	public void testToString() {

		handCards.add(twoSpades);
		handCards.add(aceHearts);
		handCards.add(aceSpades);
		handCards.add(aceClubs);
		handCards.add(fiveSpades);

		handCards.toString();
	}

	/**
	 * Test to see if IsFlush method works
	 */
	@Test
	public void testIsFlush() {

		handCards.add(twoSpades);
		handCards.add(fourSpades);
		handCards.add(fiveSpades);
		handCards.add(aceSpades);
		handCards.add(threeSpades);

		assertThat(handCards.isFlush(), equalTo(true));
	}
	
	/**
	 * Test to see if ThreeOfAKind method works
	 */
	@Test
	public void testThreeOfAKind() {

		handCards.add(aceHearts);
		handCards.add(fourSpades);
		handCards.add(fiveSpades);
		handCards.add(aceSpades);
		handCards.add(aceClubs);

		assertThat(handCards.ThreeOfAKind(), equalTo(true));
	}

}