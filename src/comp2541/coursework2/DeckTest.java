package comp2541.coursework2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;

/**
 * @author Dimitar Arabadzhiyski
 *
 */
public class DeckTest {

	private Deck allCards = new Deck();

	private Card aceClubs;
	private Card twoClubs;
	private Card twoDiamonds;
	private Card jackHearts;
	private Card kingSpades;

	/**
	 * Setting up cards for tests
	 */
	@Before
	public void setUp() {
		aceClubs = new Card("AC");
		twoClubs = new Card("2C");
		twoDiamonds = new Card("2D");
		jackHearts = new Card("JH");
		kingSpades = new Card("KS");
	}

	/**
	 * Checks that a deck of 52 cards is created when instantiating a Deck
	 * object
	 */
	@Test
	public void testCreation() {
		Deck instance = new Deck();
		assertEquals(52, instance.size());
	}

	/**
	 * Checks that the contains method behaves as expected
	 */
	@Test
	public void testContains() {
		assertTrue(allCards.contains(aceClubs) == true);
		assertTrue(allCards.contains(twoClubs) == true);
		assertTrue(allCards.contains(twoDiamonds) == true);
		assertTrue(allCards.contains(jackHearts) == true);
		assertTrue(allCards.contains(kingSpades) == true);

	}

	/**
	 * Checks that deal removes the expected card and reduces the size of the
	 * deck
	 */
	@Test
	public void testDeal() {
		assertThat(allCards.deal(), equalTo(aceClubs));
		assertThat(allCards.deal(), equalTo(twoClubs));
		assertTrue(allCards.size() < 52);
	}

	/**
	 * Checks that adding a card succeeds when the card is not already present
	 * in the deck
	 */
	@Test
	public void testAdd() {
		allCards.deal();
		assertThat(allCards.contains(aceClubs), equalTo(false));
		allCards.add(aceClubs);
		assertThat(allCards.contains(aceClubs), equalTo(true));
	}

	/**
	 * Checks that adding a card fails by throwing IllegalArgumentException when
	 * the card is already in the deck
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testFailedAdd() {

		allCards.add(jackHearts);

	}

}
