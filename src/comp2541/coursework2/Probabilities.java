package comp2541.coursework2;

/**
 * @author Dimitar Arabadzhiyski
 *
 */

public class Probabilities {
	private static Deck deck;
	private static PokerHand pokerHand;

	/**
	 * 
	 * Helps to calculate the percent of combination occurrences
	 */
	public static double percent(int part, int total) {
		return (double) part * 100 / (double) total;
	}

	/**
	 * 
	 * Main method that creates 100000 different poker hands and calculates the occurrences of different combinations 
	 */
	public static void main(String[] args) {

		int count = 0;
		int Flush = 0;
		int threeOfAKind = 0;
		int total = 10000;

		for (int i = 0; i < 10000; ++i) {
			deck = new Deck();
			for (int j = 0; j < 10; ++j) {
				pokerHand = new PokerHand();
				for (int k = 0; k < 5; ++k) {

					deck.shuffle();
					pokerHand.add(deck.deal());

				}
				count++;

			}
			if (pokerHand.isFlush() == true) {
				Flush++;

			} else if (pokerHand.ThreeOfAKind() == true) {
				threeOfAKind++;
			}

		}
		System.out.println(count + " hands dealt.");
		System.out.println("Flush occurred " + Flush + " times,");
		System.out.println("Three of A Kind occurred " + threeOfAKind + " times,");
		System.out.println("Estimated P(Flush) = " + percent(Flush, total) + "%");
		System.out.println("Estimated P(ThreeOfAKind) = " + percent(threeOfAKind, total) + "%");

	}
}