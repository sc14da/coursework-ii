package comp2541.coursework2;

/**
 * @author Dimitar Arabadzhiyski
 *
 */
public class PokerHand extends CardCollection {

	/**
	 * This method throws an illegalargument if there are already 5 cards drown
	 */
	public void add(Card card) {

		if (cards.size() == 5) {
			throw new IllegalArgumentException("Card is present!");
		} else {
			cards.add(card);
		}
	}

	/**
	 * Returns "<empty>" if there are no cards in the hand, otherwise it returns
	 * a string of the cards
	 */
	@Override
	public String toString() {

		String stringCards = new String();

		if (cards.isEmpty() == true) {
			return "<empty>";
		} else {
			for (int i = 0; i < cards.size(); i++) {
				stringCards += cards.get(i) + " ";
			}
			return stringCards;
		}
	}

	/**
	 * Checks if the poker hand is flush and return true if it is
	 * 
	 * @return false if it is not
	 */
	public boolean isFlush() {
		int count = 1;
		for (int i = 0; i < cards.size(); ++i) {
			if (count == 5) {
				return true;

			}
			for (int j = 0; j < cards.size(); j++) {
				if (cards.get(i).getSuit() == cards.get(j).getSuit()) {
					count++;

				}
			}
		}
		return false;
	}

	/**
	 * Returns true if the poker hand has three of a kind
	 * 
	 * @return false otherwise
	 */
	public boolean ThreeOfAKind() {
		boolean flag = false;
		int count = 0;
		String hand = this.toString();
		String trimmed = hand.replaceAll("\\s", "");
		char element = trimmed.charAt(0);
		for (int i = 0; i < trimmed.length(); i = i + 2) {
			if (trimmed.charAt(i) == element) {
				count++;
			}
		}
		if (count == 3) {
			flag = true;
		}

		return flag;
	}

}
